# Symfony 5 API running on Docker


## How to run the app

#### Navigate to the docker folder of this repository:

```
cd docker
```

#### Start the docker containers:

```
docker-compose up
```

#### Run composer install to install external libraries:

```
docker-compose run php-fpm composer install
```

#### Run the migrations:
TODO: move these to run after composer install

```
docker-compose run php-fpm bin/console doctrine:migratins:migrate
```

#### Run the fixtures:

```
docker-compose run php-fpm bin/console doctrine:fixtures:load -n
```

#### Navigate to http://localhost/{endpoint}
See `docs/open-api.yaml` for endpoints

#### Run the tests:

```
docker-compose run php-fpm bin/phpunit
```

## Assumptions

- Docker needs to be installed on the machine
- The repository was cloned on the machine


## Todo:
- Endpoints for Family, CommodityClass and Commodity need to be finalized and documented
- Tests are needed for above endpoints, Segment endpoints except getAll, unit tests for repository functions
- Add renderer for open-api spec. Redoc or something similar
- Implement code quality tools: PHPStan and linter