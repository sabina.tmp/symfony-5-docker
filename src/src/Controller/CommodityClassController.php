<?php

namespace App\Controller;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Repository\SegmentRepository;
use Symfony\Component\Serializer\SerializerInterface;

class CommodityClassController extends BaseController
{
    /**
     * @var SegmentRepository
     */
    private $commodityClassRepository;

    public function __construct(
        SegmentRepository $commodityClassRepository,
        SerializerInterface $serializer
    ) {
        $this->commodityClassRepository = $commodityClassRepository;

        parent::__construct($serializer);
    }

    /**
     * @Route("/commodity-classes", methods={"GET"})
     */
    public function getAll()
    {
        $segments = $this->commodityClassRepository->findAll();

        return new JsonResponse($segments);
    }

    /**
     * @Route("/commodity-classes/{id}", methods={"GET"})
     */
    public function getOne(int $id)
    {
        $segment = $this->commodityClassRepository->find($id);

        return new JsonResponse($segment);
    }

    /**
     * @Route("/commodity-classes", methods={"POST"})
     */
    public function create()
    {
        $segment = $this->commodityClassRepository->findAll();

        return new JsonResponse($segment);
    }

    /**
     * @Route("/commodity-classes/{id}", methods={"PUT"})
     */
    public function update(int $id)
    {
        $segment = $this->commodityClassRepository->find($id);

        return new JsonResponse(null);
    }

    /**
     * @Route("/commodity-classes/{id}", methods={"DELETE"})
     */
    public function delete(int $id)
    {
        $this->commodityClassRepository->remove($id);

        return new JsonResponse(null, Response::HTTP_ACCEPTED);
    }
}
