<?php

namespace App\Controller;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Repository\SegmentRepository;
use Symfony\Component\Serializer\SerializerInterface;

class CommodityController extends BaseController
{
    /**
     * @var SegmentRepository
     */
    private $commodityRepository;

    public function __construct(
        SegmentRepository $familyRepository,
        SerializerInterface $serializer
    ) {
        $this->commodityRepository = $familyRepository;

        parent::__construct($serializer);
    }

    /**
     * @Route("/commodities", methods={"GET"})
     */
    public function getAll()
    {
        $segments = $this->commodityRepository->findAll();

        return new JsonResponse($segments);
    }

    /**
     * @Route("/commodities/{id}", methods={"GET"})
     */
    public function getOne(int $id)
    {
        $segment = $this->commodityRepository->find($id);

        return new JsonResponse($segment);
    }

    /**
     * @Route("/commodities", methods={"POST"})
     */
    public function create()
    {
        $segment = $this->commodityRepository->findAll();

        return new JsonResponse($segment);
    }

    /**
     * @Route("/commodities/{id}", methods={"PUT"})
     */
    public function update(int $id)
    {
        $item = $this->commodityRepository->find($id);

        return new JsonResponse(null);
    }

    /**
     * @Route("/commodities/{id}", methods={"DELETE"})
     */
    public function delete(int $id)
    {
        $this->commodityRepository->remove($id);

        return new JsonResponse(null, Response::HTTP_ACCEPTED);
    }
}
