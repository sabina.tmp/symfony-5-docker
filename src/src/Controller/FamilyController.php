<?php

namespace App\Controller;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Repository\SegmentRepository;
use Symfony\Component\Serializer\SerializerInterface;

class FamilyController extends BaseController
{
    /**
     * @var SegmentRepository
     */
    private $familyRepository;

    public function __construct(
        SegmentRepository $familyRepository,
        SerializerInterface $serializer
    )
    {
        $this->familyRepository = $familyRepository;

        parent::__construct($serializer);
    }

    /**
     * @Route("/families", methods={"GET"})
     */
    public function getAll()
    {
        $segments = $this->familyRepository->findAll();

        return new JsonResponse($segments);
    }

    /**
     * @Route("/families/{id}", methods={"GET"})
     */
    public function getOne(int $id)
    {
        $segment = $this->familyRepository->find($id);

        return new JsonResponse($segment);
    }

    /**
     * @Route("/families", methods={"POST"})
     */
    public function create()
    {
        $segment = $this->familyRepository->findAll();

        return new JsonResponse($segment);
    }

    /**
     * @Route("/families/{id}", methods={"PUT"})
     */
    public function update(int $id)
    {
        $segment = $this->familyRepository->find($id);

        return new JsonResponse(null);
    }

    /**
     * @Route("/families/{id}", methods={"DELETE"})
     */
    public function delete(int $id)
    {
        $this->familyRepository->remove($id);

        return new JsonResponse(null, Response::HTTP_ACCEPTED);
    }
}
