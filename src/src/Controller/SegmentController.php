<?php

namespace App\Controller;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use App\Repository\SegmentRepository;
use Symfony\Component\Serializer\SerializerInterface;

class SegmentController extends BaseController
{
    /** @var SegmentRepository */
    private $segmentRepository;


    public function __construct(
        SegmentRepository $segmentRepository,
        SerializerInterface $serializer
    ) {
        $this->segmentRepository = $segmentRepository;

        parent::__construct($serializer);
    }

    /**
     * @Route("/segments", methods={"GET"})
     */
    public function getAll(): Response
    {
        $segments = $this->segmentRepository->findAll();

        // TODO: replace with serializer
        $response = [];
        foreach ($segments as $segment) {
            $response[] = [
                'id' => $segment->getId(),
                'code' => $segment->getCode(),
                'name' => $segment->getName(),
            ];
        }

        return $this->createResponse($response);
    }

    /**
     * @Route("/segments/{id}", methods={"GET"})
     */
    public function getOne(int $id)
    {
        $segment = $this->segmentRepository->find($id);
        if (!$segment) {
            throw new NotFoundHttpException();
        }

        $response = [
            'id' => $segment->getId(),
            'code' => $segment->getCode(),
            'name' => $segment->getName(),
        ];

        return $this->createResponse($response);
    }

    /**
     * @Route("/segments", methods={"POST"})
     */
    public function create(Request $request)
    {
        $data = $request->request->all();
        $this->segmentRepository->create($data);

        return $this->createResponse([], Response::HTTP_CREATED);
    }

    /**
     * @Route("/segments/{id}", methods={"PUT"})
     */
    public function update(int $id, Request $request)
    {
        $data = json_decode($request->getContent(), true);
        $this->segmentRepository->update($id, $data);

        return new Response(null, Response::HTTP_NO_CONTENT);
    }

    /**
     * @Route("/segments/{id}", methods={"DELETE"})
     */
    public function delete(int $id): Response
    {
        $this->segmentRepository->remove($id);

        return new Response(null, Response::HTTP_NO_CONTENT);
    }
}
