<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use App\Entity\{Commodity, CommodityClass, Family, Segment};

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        // increase memory limit for this script
        // hacky. should be done by increasing php cli memory limit in the docker container php.ini settings

        // todo: get a proper CSV importer and process in batches if needed.
        ini_set('memory_limit', '512MB');

        // todo: this should be a parameter
        $fileName = __DIR__ . '/../Resource/data.csv';

        $row = 0;
        if (($handle = fopen($fileName, 'r')) !== FALSE) {
            while (($data = fgetcsv($handle)) !== FALSE) {
                $row++;

                // skip first row: the header
                if ($row === 1) {
                    continue;
                }

                // quick data check
                if (count($data) < 8) {
                    // todo: log malformed row
                    // todo: move to processRow function and throw an error there and catch it here

                    continue;
                }

                $this->processRow($manager, $data);
            }
            $manager->flush();

            fclose($handle);
        }
    }

    /**
     * @param ObjectManager $manager
     * @param $data
     */
    private function processRow(ObjectManager $manager, $data): void
    {
        /** @var Segment $segment */
        $segment = $this->getSegment($manager, $data[0], $data[1]);

        /** @var Family $family */
        $family = $this->getFamily($manager, $data[2], $data[3]);
        $family->setSegment($segment);

        /** @var CommodityClass $commodityClass */
        $commodityClass = $this->getCommodityClass($manager, $data[4], $data[5]);
        $commodityClass->setFamily($family);

        /** @var Commodity $commodity */
        $commodity = $this->getCommodity($manager, $data[6], $data[7]);
        $commodity->setCommodityClass($commodityClass);
    }

    /**
     * Finds a segment if it already exists in the database, else it creates it
     * @param ObjectManager $manager
     * @param string $code
     * @param string $name
     * @return Segment
     */
    private function getSegment(ObjectManager $manager, string $code, string $name): Segment
    {
        $segment = $manager->getRepository(Segment::class)->findOneBy([
            'code' => $code,
            'name' => $name
        ]);

        if (!$segment) {

            $segment = new Segment();
            $segment->setCode($code);
            $segment->setName($name);

            $manager->persist($segment);
            $manager->flush(); // we need to flush, otherwise it won't be saved in the database until the end
        }

        return $segment;
    }

    /**
     * Finds a family if it already exists in the database, else it creates it
     * @param ObjectManager $manager
     * @param string $code
     * @param string $name
     * @return Family
     */
    private function getFamily(ObjectManager $manager, string $code, string $name): Family
    {
        $family = $manager->getRepository(Family::class)->findOneBy([
            'code' => $code,
            'name' => $name
        ]);

        if (!$family) {
            $family = new Family();
            $family->setCode($code);
            $family->setName($name);

            $manager->persist($family);
            $manager->flush();
        }

        return $family;
    }

    /**
     * Finds a class if it already exists in the database, else it creates it
     * @param ObjectManager $manager
     * @param string $code
     * @param string $name
     * @return CommodityClass
     */
    private function getCommodityClass(ObjectManager $manager, string $code, string $name): CommodityClass
    {
        $commodityClass = $manager->getRepository(CommodityClass::class)->findOneBy([
            'code' => $code,
            'name' => $name
        ]);

        if (!$commodityClass) {
            $commodityClass = new CommodityClass();
            $commodityClass->setCode($code);
            $commodityClass->setName($name);

            $manager->persist($commodityClass);
            $manager->flush();
        }

        return $commodityClass;
    }


    /**
     * Finds a commodity if it already exists in the database, else it creates it
     * @param ObjectManager $manager
     * @param  string $code
     * @param string $name
     * @return Commodity
     */
    private function getCommodity(ObjectManager $manager, string $code, string $name): Commodity
    {
        // if fixtures are only ran once and correctly, then this should NOT return anything
        $commodity = $manager->getRepository(Commodity::class)->findOneBy([
            'code' => $code,
            'name' => $name
        ]);

        // but it's safer to check
        if (!$commodity) {
            $commodity = new Commodity();
            $commodity->setCode($code);
            $commodity->setName($name);

            $manager->persist($commodity);
            $manager->flush();
        }

        return $commodity;
    }
}
