<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CommodityRepository")
 * @ORM\Table(name="commodities", uniqueConstraints={@ORM\UniqueConstraint(columns={"code", "name"})})
 */
class Commodity
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=10, nullable=false)
     * @var string
     */
    private $code;

    /**
     * @ORM\Column(type="string", length=255, nullable=false)
     * @var string
     */
    private $name;

    /**
     * Owning side of Many to one relationship
     * @ORM\ManyToOne(targetEntity="CommodityClass")
     * @ORM\JoinColumn(name="commodity_class_id", referencedColumnName="id")
     * @var CommodityClass
     */
    private $commodityClass;

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getCode(): string
    {
        return $this->code;
    }

    /**
     * @param string $code
     * @return self
     */
    public function setCode(string $code): self
    {
        $this->code = $code;
        return $this;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return self
     */
    public function setName(string $name): self
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return CommodityClass
     */
    public function getCommodityClass(): CommodityClass
    {
        return $this->commodityClass;
    }

    /**
     * @param CommodityClass $commodityClass
     */
    public function setCommodityClass(CommodityClass $commodityClass): self
    {
        $this->commodityClass = $commodityClass;
        return $this;
    }
}