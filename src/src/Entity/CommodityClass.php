<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CommodityClassRepository")
 * @ORM\Table(name="commodity_classes", uniqueConstraints={@ORM\UniqueConstraint(columns={"code", "name"})})
 */
class CommodityClass
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=10, nullable=false)
     */
    private $code;

    /**
     * @ORM\Column(type="string", length=255, nullable=false)
     */
    private $name;

    /**
     * Inverse side of Commodity:commodityClass
     * @ORM\OneToMany(targetEntity="Commodity", mappedBy="commodityClass")
     * @var Commodity[]|Collection
     */
    private $commodities;

    /**
     * Owning side of Many to one relationship
     * @ORM\ManyToOne(targetEntity="Family")
     * @ORM\JoinColumn(name="family_id", referencedColumnName="id")
     * @var Family
     */
    private $family;

    public function __construct()
    {
        $this->commodities = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getCode(): string
    {
        return $this->code;
    }

    /**
     * @param string $code
     * @return self
     */
    public function setCode(string $code): self
    {
        $this->code = $code;
        return $this;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return self
     */
    public function setName(string $name): self
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return Commodity[]|Collection
     */
    public function getCommodities(): Collection
    {
        return $this->commodities;
    }

    /**
     * @param Commodity[]|Collection $commodities
     * @return CommodityClass
     */
    public function setCommodities(Collection $commodities)
    {
        $this->commodities = $commodities;
        return $this;
    }

    /**
     * @return Family
     */
    public function getFamily(): Family
    {
        return $this->family;
    }

    /**
     * @param Family $family
     * @return CommodityClass
     */
    public function setFamily(Family $family): self
    {
        $this->family = $family;
        return $this;
    }
}