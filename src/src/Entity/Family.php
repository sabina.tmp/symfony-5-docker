<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\FamilyRepository")
 * @ORM\Table(name="families", uniqueConstraints={@ORM\UniqueConstraint(columns={"code", "name"})})
 */
class Family
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=10, nullable=false)
     */
    private $code;

    /**
     * @ORM\Column(type="string", length=255, nullable=false)
     */
    private $name;

    /**
     * Inverse side
     * @ORM\OneToMany(targetEntity="CommodityClass", mappedBy="family")
     * @var CommodityClass[]|Collection
     */
    private $commodityClasses;

    /**
     * Owning side of Many to one relationship
     * @ORM\ManyToOne(targetEntity="Segment")
     * @ORM\JoinColumn(name="segment_id", referencedColumnName="id")
     * @var Segment
     */
    private $segment;

    public function __construct()
    {
        $this->commodityClasses = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getCode(): string
    {
        return $this->code;
    }

    /**
     * @param string $code
     * @return self
     */
    public function setCode(string $code): self
    {
        $this->code = $code;
        return $this;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return self
     */
    public function setName(string $name): self
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return CommodityClass[]|Collection
     */
    public function getCommodityClasses(): Collection
    {
        return $this->commodityClasses;
    }

    /**
     * @param CommodityClass[]|Collection $commodityClasses
     * @return Family
     */
    public function setCommodityClasses(Collection $commodityClasses)
    {
        $this->commodityClasses = $commodityClasses;
        return $this;
    }

    /**
     * @return Segment
     */
    public function getSegment(): Segment
    {
        return $this->segment;
    }

    /**
     * @param Segment $segment
     * @return Family
     */
    public function setSegment(Segment $segment): self
    {
        $this->segment = $segment;
        return $this;
    }
}