<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\SegmentRepository")
 * @ORM\Table(name="segments", uniqueConstraints={@ORM\UniqueConstraint(columns={"code", "name"})})
 */
class Segment
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=10, nullable=false)
     */
    private $code;

    /**
     * @ORM\Column(type="string", length=255, nullable=false)
     */
    private $name;

    /**
     * Inverse side
     * @ORM\OneToMany(targetEntity="Family", mappedBy="segment")
     * @var Family[]|Collection
     */
    private $families;

    public function __construct()
    {
        $this->families = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getCode(): string
    {
        return $this->code;
    }

    /**
     * @param string $code
     * @return self
     */
    public function setCode(string $code): self
    {
        $this->code = $code;
        return $this;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return self
     */
    public function setName(string $name): self
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return Family[]|Collection
     */
    public function getFamilies(): Collection
    {
        return $this->families;
    }

    /**
     * @param Family[]|Collection $families
     * @return Segment
     */
    public function setFamilies(Collection $families)
    {
        $this->families = $families;
        return $this;
    }
}
