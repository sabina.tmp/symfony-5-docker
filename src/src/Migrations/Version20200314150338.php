<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200314150338 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE families ADD segment_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE families ADD CONSTRAINT FK_995F3FCCDB296AAD FOREIGN KEY (segment_id) REFERENCES segments (id)');
        $this->addSql('CREATE INDEX IDX_995F3FCCDB296AAD ON families (segment_id)');
        $this->addSql('ALTER TABLE commodities ADD commodity_class_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE commodities ADD CONSTRAINT FK_6CFBD1CD969C4EE6 FOREIGN KEY (commodity_class_id) REFERENCES commodity_classes (id)');
        $this->addSql('CREATE INDEX IDX_6CFBD1CD969C4EE6 ON commodities (commodity_class_id)');
        $this->addSql('ALTER TABLE commodity_classes ADD family_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE commodity_classes ADD CONSTRAINT FK_42473F0BC35E566A FOREIGN KEY (family_id) REFERENCES families (id)');
        $this->addSql('CREATE INDEX IDX_42473F0BC35E566A ON commodity_classes (family_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE commodities DROP FOREIGN KEY FK_6CFBD1CD969C4EE6');
        $this->addSql('DROP INDEX IDX_6CFBD1CD969C4EE6 ON commodities');
        $this->addSql('ALTER TABLE commodities DROP commodity_class_id');
        $this->addSql('ALTER TABLE commodity_classes DROP FOREIGN KEY FK_42473F0BC35E566A');
        $this->addSql('DROP INDEX IDX_42473F0BC35E566A ON commodity_classes');
        $this->addSql('ALTER TABLE commodity_classes DROP family_id');
        $this->addSql('ALTER TABLE families DROP FOREIGN KEY FK_995F3FCCDB296AAD');
        $this->addSql('DROP INDEX IDX_995F3FCCDB296AAD ON families');
        $this->addSql('ALTER TABLE families DROP segment_id');
    }
}
