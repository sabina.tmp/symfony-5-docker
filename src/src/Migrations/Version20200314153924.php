<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200314153924 extends AbstractMigration
{
    public function getDescription() : string
    {
        return 'add code fields and unique constraints';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE segments ADD code VARCHAR(10) NOT NULL');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_26CEDB29771530985E237E06 ON segments (code, name)');
        $this->addSql('ALTER TABLE families ADD code VARCHAR(10) NOT NULL, CHANGE segment_id segment_id INT DEFAULT NULL');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_995F3FCC771530985E237E06 ON families (code, name)');
        $this->addSql('ALTER TABLE commodities ADD code VARCHAR(10) NOT NULL, CHANGE commodity_class_id commodity_class_id INT DEFAULT NULL');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_6CFBD1CD771530985E237E06 ON commodities (code, name)');
        $this->addSql('ALTER TABLE commodity_classes ADD code VARCHAR(10) NOT NULL, CHANGE family_id family_id INT DEFAULT NULL');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_42473F0B771530985E237E06 ON commodity_classes (code, name)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP INDEX UNIQ_6CFBD1CD771530985E237E06 ON commodities');
        $this->addSql('ALTER TABLE commodities DROP code, CHANGE commodity_class_id commodity_class_id INT DEFAULT NULL');
        $this->addSql('DROP INDEX UNIQ_42473F0B771530985E237E06 ON commodity_classes');
        $this->addSql('ALTER TABLE commodity_classes DROP code, CHANGE family_id family_id INT DEFAULT NULL');
        $this->addSql('DROP INDEX UNIQ_995F3FCC771530985E237E06 ON families');
        $this->addSql('ALTER TABLE families DROP code, CHANGE segment_id segment_id INT DEFAULT NULL');
        $this->addSql('DROP INDEX UNIQ_26CEDB29771530985E237E06 ON segments');
        $this->addSql('ALTER TABLE segments DROP code');
    }
}
