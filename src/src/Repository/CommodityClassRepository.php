<?php

namespace App\Repository;

use App\Entity\CommodityClass;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method CommodityClass|null find($id, $lockMode = null, $lockVersion = null)
 * @method CommodityClass|null findOneBy(array $criteria, array $orderBy = null)
 * @method CommodityClass[]    findAll()
 * @method CommodityClass[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CommodityClassRepository extends ServiceEntityRepository
{
    use DeleteTrait;

    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, CommodityClass::class);
    }
}
