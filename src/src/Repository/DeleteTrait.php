<?php


namespace App\Repository;


trait DeleteTrait
{
    /**
     * @param $id
     */
    public function delete($id): void
    {
        $entity = $this->find($id);

        $em = $this->getEntityManager();
        $em->remove($entity);
        $em->flush();
    }
}