<?php

namespace App\Repository;

use App\Entity\Segment;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * @method Segment|null find($id, $lockMode = null, $lockVersion = null)
 * @method Segment|null findOneBy(array $criteria, array $orderBy = null)
 * @method Segment[]    findAll()
 * @method Segment[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SegmentRepository extends ServiceEntityRepository
{
    use DeleteTrait;

    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Segment::class);
    }

    public function create(array $data): Segment
    {
        $segment = new Segment();
        $segment->setCode($data['code']);
        $segment->setName($data['name']);

        $em = $this->getEntityManager();
        $em->persist($segment);
        $em->flush();

        return $segment;
    }

    public function update(int $id, array $data): Segment
    {
        $segment = $this->find($id);
        if (!$segment) {
            throw new NotFoundHttpException();
        }

        $segment->setCode($data['code']);
        $segment->setName($data['name']);

        $em = $this->getEntityManager();
        $em->persist($segment);
        $em->flush();

        return $segment;
    }

    public function remove(int $id)
    {
        $item = $this->find($id);
        if(!$item) {
            throw new NotFoundHttpException();
        }

        if ($item->getFamilies()->count() > 0) {
            throw new \Exception('This Segment has Families related to it. Please remove them first');
        }

        $em = $this->getEntityManager();
        $em->remove($item);
        $em->flush();
    }

}
