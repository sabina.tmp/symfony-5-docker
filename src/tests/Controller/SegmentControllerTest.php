<?php


namespace App\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class SegmentControllerTest extends WebTestCase
{
    public function testCanGetAllSegments()
    {
        $client = static::createClient();

        $client->request('GET', '/segments');
        $this->assertTrue($client->getResponse()->isSuccessful());

        $response = json_decode($client->getResponse()->getContent());

        $this->assertGreaterThan(0, $response);

        foreach ($response as $item) {
            $this->assertObjectHasAttribute('id', $item);
            $this->assertObjectHasAttribute('code', $item);
            $this->assertObjectHasAttribute('name', $item);
        }
    }

}